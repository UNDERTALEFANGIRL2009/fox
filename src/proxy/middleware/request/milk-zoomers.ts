import { isCompletionRequest } from "../common";
import { ProxyRequestMiddleware } from ".";

const DISALLOWED_ORIGIN_SUBSTRINGS = "janitorai.com,janitor.ai".split(",");

class ForbiddenError extends Error {
  constructor(message: string) {
    super(message);
    this.name = "ForbiddenError";
  }
}

/**
 * taking money from idiots the long way
 */
export const milkZoomers: ProxyRequestMiddleware = (_proxyReq, req) => {
  if (!isCompletionRequest(req)) {
    return;
  }

  const origin = req.headers.origin || req.headers.referer;
  if (origin && DISALLOWED_ORIGIN_SUBSTRINGS.some((s) => origin.includes(s))) {
    // Venus-derivatives send a test prompt to check if the proxy is working.
    // We don't want to block that just yet.
    if (req.body.messages[0]?.content === "Just say TEST") {
      return;
    }

    // Math.random returns between a 0 and a 1. 0.6 = 60% chance to pass. 
    // Probably should make the chance lower after a while to not raise suspicion.
    if (Math.random() <= 0.6) {
      throw new ForbiddenError(
        `API responded with Error 503: SERVER OVERLOADED. PLEASE TRY AGAIN IN A MINUTE. Note from the devs: Please help us pay for server costs, even a mere $5 from each of you could help run the server for a year uninterrupted! https://ko-fi.com/knolastname`
      );
    } else return;
  }
};
